﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class PlayerName : MonoBehaviour
{
    /* This class is setting up the player name. 
     * 
     * Copyrigth Animgram Aps 2021, developed by Tom Holme Vedel
     */

    [SerializeField] InputField _inputPlayerName;     // reference to the object holding the screen to enter playername
    [SerializeField] GameObject _mainMenu;             // reference to the object holding the main menu
 
    void Start()
    {
        if (PlayerPrefs.GetString("playerName") == null)
            PlayerPrefs.SetString("playerName", "Player Name");

        _inputPlayerName.text = PlayerPrefs.GetString("playerName");
    }
    public void OnButtonPlayerNameEnter()         // If the app has not registered a player name, it will ask for one.
    {
        PlayerPrefs.SetString("playerName", _inputPlayerName.text);
        _mainMenu.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
