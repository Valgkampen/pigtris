﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateApp : MonoBehaviour
{
    public GameObject configManagerObject;
    MainConfigurationManager configManager;
    public GameObject introscreen;
    public GameObject background;
    // Start is called before the first frame update
    private void Start()
    {
        introscreen.SetActive(false);
        background.SetActive(true);
        Time.timeScale = 0f;
        configManager = configManagerObject.GetComponent<MainConfigurationManager>();
    }
    void Update()
    {
        {
            if (Input.GetMouseButtonDown(0))
            {
                Application.OpenURL(configManager.updateLink);
            }
        }
    }
}
