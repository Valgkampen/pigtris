using UnityEngine;
using System.Collections;

public class SoundEffectsController : SoundController
{
    /* This class ....
     * 
     * 
     * Copyrigth Animgram Aps 2020, developed by Tom Vedel
     */

    #region singleton implementation

       private static SoundEffectsController instance;
       public static SoundEffectsController Instance { get { return instance; } }

    #endregion

    public AudioClip mainSound;
    public AudioClip[] Grynts;
    public AudioClip[] Hyl;
    public AudioClip[] Prutter;


    private AudioSource effectSource1; 
    private AudioSource effectSource2;
    private AudioSource loopSource;
    private float previousVolume;

    void Start()
    {
        instance = this;
        effectSource1 = gameObject.AddComponent<AudioSource>();
        effectSource1.loop = false;

        effectSource2 = gameObject.AddComponent<AudioSource>();
        effectSource2.loop = false;

        loopSource = gameObject.AddComponent<AudioSource>();
        loopSource.loop = true;
        loopSource.volume = 0.7f;
        previousVolume = loopSource.volume;

        PlaySound(mainSound, loopSource);
    }
    public void playGrynt()
    {
        PlayOneShotRandomSound(Grynts, effectSource1);
    }
    public void playStart()
    {
        PlaySound(mainSound, loopSource);
    } 
    public void playHyl()
    {
        PlayOneShotRandomSound(Hyl, effectSource2);
    }
    public void playPrutter()
    {
        PlayOneShotRandomSound(Prutter, effectSource1);
    }
    public void stopMusic()
    {
        StartCoroutine("FadeOut");
    }
    public void silence()
    {
        previousVolume = loopSource.volume;
        loopSource.volume = 0.0f;

    }
    public void setVolume()
    {
        loopSource.volume = 0.7f;
    }

    protected IEnumerator FadeOut()
    {
        float startVolume = loopSource.volume;
        while (loopSource.volume > 0)
        {
            loopSource.volume -= startVolume * 0.025f;
            yield return new WaitForSeconds(0.1f);
        }
        loopSource.Stop();
        loopSource.volume = startVolume;
    }
}
