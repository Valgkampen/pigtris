﻿using UnityEngine;
using System.Collections;
using System.IO;
public class IntroManager : MonoBehaviour
{
    string _filePathLanguageVersion;
    string _filePathImage;
    LanguageVersion _languageVersion;
    [SerializeField] string _hitLink;
    [SerializeField] GameObject _background;
    [SerializeField] GameObject _setPlayerName;
    [SerializeField] GameObject _mainMenu;
    private void OnEnable()
    {
        _filePathLanguageVersion = Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameLanguageVersion;

        if (File.Exists(_filePathLanguageVersion))
        {
            StartCoroutine(IntroAnimgramercial());
        }
        else
        {
            StartCoroutine(DefaultAnimgramercial());
        }
    }
    void Update()
    {
        {
            if (Input.GetMouseButtonDown(0))
            {
                Application.OpenURL(_hitLink);
            }
        }
    }
    IEnumerator IntroAnimgramercial()
    {
        _languageVersion = JsonUtility.FromJson<LanguageVersion>(System.IO.File.ReadAllText(_filePathLanguageVersion));

        yield return new WaitForSecondsRealtime(ShowAnimgramImage());
         if (PlayerPrefs.GetString("playerName") == "")
         _setPlayerName.SetActive(true);
        else
            _mainMenu.SetActive(true);
        _background.SetActive(false);
        this.gameObject.SetActive(false);
        Time.timeScale = 1f;
        yield return null;
    }        
    int ShowAnimgramImage()
    {
        Texture2D tex2D;
        byte[] fileData;
        int i = UnityEngine.Random.Range(0, _languageVersion.AnimgramInfoImages.Length);

        _filePathImage = Application.persistentDataPath +"/" + Constants.appId + "/Images/" + _languageVersion.AnimgramInfoImages[i].ImageFileName;
        if (File.Exists(_filePathImage))
        {
            fileData = File.ReadAllBytes(_filePathImage);
            tex2D = new Texture2D(Constants.imageLength, Constants.imageHeight);
            if (tex2D.LoadImage(fileData))
            {
                GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex2D, new Rect(0, 0, Constants.imageLength, Constants.imageHeight), new Vector2(0, 0));
#if UNITY_ANDROID
                _hitLink = _languageVersion.AnimgramInfoImages[i].URLAndroid;
#elif UNITY_IPHONE
                _hitLink = _languageVersion.AnimgramInfoImages[i].URLIos;
#else           
                _hitLink = "https://animgram.com/";
#endif
            }
        }
        return _languageVersion.AnimgramInfoImages[i].Duration;
    }
    IEnumerator DefaultAnimgramercial()
    {
 
#if UNITY_ANDROID
        string filePathDefaultImage = System.IO.Path.Combine(Application.streamingAssetsPath, Constants.defaultImageName);
        var unityWebRequestTexture = UnityEngine.Networking.UnityWebRequestTexture.GetTexture(filePathDefaultImage);
        yield return unityWebRequestTexture.SendWebRequest();
        var fileData = unityWebRequestTexture.downloadHandler.data;
        var tex2D = new Texture2D(Constants.imageLength, Constants.imageHeight);

        if (tex2D.LoadImage(fileData))
        {
            GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex2D, new Rect(0, 0, Constants.imageLength, Constants.imageHeight), new Vector2(0, 0));
            _hitLink = "https://www.animgram.com";
        }
#else
        string filePathDefaultImage = System.IO.Path.Combine(Application.streamingAssetsPath, Constants.defaultImageName);
        Texture2D tex2D;
        byte[] fileData;
        if (File.Exists(filePathDefaultImage))
        {
            fileData = System.IO.File.ReadAllBytes(filePathDefaultImage);
            tex2D = new Texture2D(Constants.imageLength, Constants.imageHeight);
            if (tex2D.LoadImage(fileData))
            {
                GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex2D, new Rect(0, 0, Constants.imageLength, Constants.imageHeight), new Vector2(0, 0));
                _hitLink = "https://www.animgram.com";
            }
        }
#endif
        yield return new WaitForSecondsRealtime(Constants.defaultWait);
        GetComponent<SpriteRenderer>().sprite = null;
         if (PlayerPrefs.GetString("playerName") == "")
         _setPlayerName.SetActive(true);
        else
            _mainMenu.SetActive(true);
        _background.SetActive(false);
        this.gameObject.SetActive(false);
        Time.timeScale = 1f;
        yield return null;
    }
}
