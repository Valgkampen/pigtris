﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public enum Swipe
{
    Tap,
    Up,
    Down,
    Left,
    Right
};

public class SwipeManager : MonoBehaviour
{
     /* This class manages the touch and mouse operationsn when the app is running.
      * The class broadcasts the events and whatever part of the app can subscribe to these events.
      * As the enum indicates the events are single taps, swipe up, down, left and rigth.
      * 
      * Copyrigth Animgram Aps 2020, developed by Tom Vedel
      */


    [Tooltip("Min swipe distance (relative pixels) to register as swipe")]
    [SerializeField] float minSwipeLength = 50f;

    [Tooltip(" The factor x needs to be larger than y to determine a horizontal swipe")]
    [SerializeField] float factorLarger = 1.2f;

    public delegate void OnSwipeDetectedHandler(Swipe swipeDir);
    public static event OnSwipeDetectedHandler OnSwipeDetected;

    static bool directionChosen;
    public static Vector2 startPos;
    public static Vector3 startPosWorld;
    public static Vector3 touchDirectionWorld;
    static Vector2 touchDirection;
    static SwipeManager instance;
    public static Swipe swipeDirection;
    private static Camera cam;
    private float screenToWorldFactor;

    void Awake()
    {
        instance = this;
        cam = Camera.main;
        screenToWorldFactor = Screen.height / 1820f;
    }

    void Update()
    {
            DetectSwipe();
    }
    static void DetectSwipe()
    /// <summary>
    /// Detect the current swipe direction.
    /// Will be called over multiple frames from touch begin to touch.Ended from Update.
    /// </summary>
    {
        if (GetTouchInput() || GetMouseInput())
        {
            // Swipe ended, process and get ready for next touch event
            if (directionChosen)
            {
                 if ((touchDirection.magnitude / instance.screenToWorldFactor) >= (instance.minSwipeLength))
                 {
                    if (Math.Abs(touchDirection.x) > Math.Abs(touchDirection.y))
                    {
                        if (touchDirection.x > 0)
                        {
                            swipeDirection = Swipe.Right;
                        }
                            else
                        {
                            swipeDirection = Swipe.Left;
                        }                          
                    }
                    else
                    {
                        if (touchDirection.y > 0)
                        {
                            swipeDirection = Swipe.Up;
                        }
                        else
                        {
                            swipeDirection = Swipe.Down;
                        }
                    }
                 }
                 else
                 {  
                    swipeDirection = Swipe.Tap;
                 }
            }
            OnSwipeDetected?.Invoke(swipeDirection);
            directionChosen = false;
            return;
        }
    }
    #region Helper Functions
    static bool GetTouchInput()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    directionChosen = false;
                    break;

                // Report that a direction has been chosen when the finger is lifted.
                case TouchPhase.Ended:
                    directionChosen = true;
                    touchDirection = new Vector2(touch.position.x - startPos.x, touch.position.y - startPos.y);
                    break;

                default:
                    directionChosen = false;
                    break;
            }
        }
        return directionChosen;
    }
    static bool GetMouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPos = (Vector2)Input.mousePosition;
             directionChosen = false;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touchDirection = new Vector2(Input.mousePosition.x - startPos.x, Input.mousePosition.y - startPos.y);
            directionChosen = true;
        }
        return directionChosen;
    }
    public static string getSwipe(Swipe swipe)
    {
        switch (swipe)
        {
            case Swipe.Tap:
                return "Tap";
            case Swipe.Up:
                return "Up";
            case Swipe.Down:
                return "Down";
            case Swipe.Left:
                return "Left";
            case Swipe.Right:
                return "Rigth";
            default:
                return "";
        }
    }
    #endregion
}
