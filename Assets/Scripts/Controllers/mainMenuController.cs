﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenuController : MonoBehaviour
{
    /* This class controls the main menu. The main menu comprises about, leaderboard, how to play
     * At some time a new menu item will be added, settings where audio options, audio volume and
     * change of name can be set.
     * 
     * Copyrigth Animgram Aps 2020, developed by Tom Vedel
     */

    public GameObject aboutMenu;            // Reference to the object holding the about menu item
    public GameObject leaderBoard;          // Reference to the object holding the leaderboard/highscore item and the highscoreController
    public GameObject instructionMenu;      // Reference to the object holding the how to play item  
    public GameObject closeAbout;           // Reference to the object holding close button for the about menu item
    public GameObject closeInstruction;     // Reference to the object holding close button for the how to play instructions      
    public GameObject closeHighScore;       // Reference to the object holding close button for the leaderboard/highscore
    public highscoreController leaderBoardController;   // Reference to the highscoreController component on the leaderBoard object
    public bool debug;
    private void Start()
    {

        leaderBoardController =  leaderBoard.GetComponent<highscoreController>();

        if (PlayerPrefs.GetInt("highScoreScreen") == 1)     // Flag, if set means start the leaderBoard menu item when the main menu loads
        {
            PlayerPrefs.SetInt("highScoreScreen", 0);
             onHighScore();
            StartCoroutine("exitLeaderboard");
        }
    }
    IEnumerator exitLeaderboard()
    {
        yield return new WaitForSeconds(4f);
        xHighScore();
    }
    #region Button Actions
    public void onAnimgramLogo()
    {
        Application.OpenURL("https://www.animgram.com/");

    }
    public void onPrivacy()
    {
        Application.OpenURL("https://www.animgram.com/privacy/");

    }

    public void onHighScore()
    {
        leaderBoardController.SelfStart();
        leaderBoardController.BuildLeaderBoard();
        leaderBoard.SetActive(true);
        closeHighScore.SetActive(true);

        }

    public void onInstructions()
    {
        instructionMenu.SetActive(true);
        closeInstruction.SetActive(true);

        }

    public void onAbout()
    {
        aboutMenu.SetActive(true);
        closeAbout.SetActive(true);

        }
    public void onPlay()
    {
        StartCoroutine("startPigtris");

        }

    public void xHighScore()
    {
        leaderBoardController.ClearLeaderBoard();
        leaderBoard.SetActive(false);
        closeHighScore.SetActive(false);

        }

    public void xInstructions()
    {
        instructionMenu.SetActive(false);
        closeInstruction.SetActive(false);

        }

    public void xAbout()
    {
        aboutMenu.SetActive(false);
        closeAbout.SetActive(false);

        }
    #endregion
    IEnumerator startPigtris()
    {
        StaticClass.mainMenuCalling = true;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Pigtris");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
