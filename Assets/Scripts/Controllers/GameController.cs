﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
   /* This class is the main control of the PigDrop/Pigtris game.
    * 
    * It has refenrences to all of the objects of the game and many objects of the game has a refernece to this. There is only 1 gamecontroller
    * and it resides on the gamecontroller object
    * 
    * The game starts by spawning a pig and as soon as it is been spawned it starts falling. 
    * 
    * The fall is controlled by the pig itself (script PigController). The pig falls in steps of 1 cell down as long as there are no obstacles. 
    * This is checked by checking the platFieldMatrix (if cell occupied or it is out of boundary). 
    * 
    * During the fall the pig can be moved left/rigth or rotated left/rigth by swipe. That too is controlled by the pigController. 
    * Again any obstacles hindring any move is controlled by checking the playfieldMatrix.    
    * Once a pig can fall no more, the Pigcontroller stops the falling routine and sets the flag pigFalling to false.
    * 
    * This gameController listens to when the pigFalling is false, then it checks whether any new complete lines are formed and if the score has reached a level
    * treshold. If so it takes appropriate action (removes full lines, calculate scores, go to next level etc.).
    * 
    * When all bookeeping has been done a new pig is spawned. If this new spawned is stuck already at spawn the pig raises the gameOver flag.
    * 
    * If gameover the total score is calculated and it is checked whether it qualifies for the leaderboard, if so a winner screen is shown for a short 
    * while followed by displaying the leaderboard screen controlled by the main menu for a short while whereafter the main menu is loaded.
    * 
    * If the score does not qualify for the leaderboard, a loser screen is displayed for a short while after which the main menu is loaded.
    * 
    *  
    * 
    * Copyrigth Animgram Aps 2020, developed by Tom Vedel
    */
 
    #region variable declarations
    private GameObject pigToProcess;        // Reference to the pig to currently process when going thru all pigs on a line checking if the line is complete
    [SerializeField] GameObject _miniPig;              // Object to show the pig to follow the one currently falling.
    [SerializeField] GameObject _highScore;          // object reference to the leaderboard/highscore
    [SerializeField] GameObject _winnerScreen;         // reference to the object holding the winnermessage screen
    [SerializeField] GameObject _loserScreen;          // reference to the object holding the winnermessage screen     
    [SerializeField] GameObject _pauseScreen;          // The object that holds the pause Screen
    [SerializeField] GameObject _pauseButton;          // Button to pause the game and activate the pause screen
    [SerializeField] GameObject _unPauseButton;        // Button to close the pause Screen and go back to game
    [SerializeField] GameObject _closeButton;          // Button to close the game panel
    [SerializeField] GameObject _gameOverText;         // Text displayed when game is over.
    public GameObject[,] _playFieldMatrix;   // Matrix (x,y) holding reference to the pig/gameobject stuck there, (extends 5 cells above the visual screen)
    [SerializeField] GameObject _scoreText;            // Object containing Text displaying the current score
    [SerializeField] GameObject _levelText;            // Object containing the the text displaying what level the game is on
    [SerializeField] GameObject _playerText;            // Object containing the name of the player currently playing
    [SerializeField] GameObject _areYouSure;           // Warning "button" to be displayed if user hist close during play.
    [SerializeField] GameObject _newLevelReached;      // Text displayed when the next level is reached, stack/playfield clears and the pigs falls at a greater speed.
    [SerializeField] GameObject[] _pigsToSpawn;                    // Array holding the different types of pigs that can fall
    [SerializeField] GameObject _adModule;
    [SerializeField] GameObject _background;
    [SerializeField] GameObject _gameBackground;
    [SerializeField] GameObject _introManager;
     
    private List<GameObject> pigsToAdjust;              //List of stucked pigs that needs te be processed for different reasons.

    public Sprite[] nextPigMiniImage;                   // Reference to the graphics of the minipigs/next pig to spawn
    
    public highscoreController _highScoreController;   // reference to the highscore controller component on the leaderboard
    private PigController pigController;                // Reference to the control script on the spawned pigs. 
    SoundEffectsController soundController;     // Reference to the controller of sound and music
    //private AdController adController;

    public int playFieldWidth = 9;                      // Width of cells on the play field
    public int playFieldHeigth = 25;                    // Height of cells on the playfield 
    public int completedLineBonus = 50;                 //Points awarded when a line is completed
    public int lineBonusMultiplikator = 4;              // Multiplier if more than 1 line is completed at the same time.
    public int extraLineBonus = 10;                     // Award for an extra line. Number of extra line (1st, 2nd, 3rd) times the multiplier times this award gives the score for the extra line 
    private int numberOfCompletedLines = 0;             // Number of completed lines in one go.
    private int fullLineNumber;                         // The y value in the playfield matrix where all cells have pigs.
    private int nextPig;                                // value between 0 and 6 referering to the index of the pigsToSpawn array. 
    private int level = 1;                              // Current level
    public int score = 0;                              // Current score
    
    public int[,] currentPigMatrix;         // Smallest matrix/rectangle that can contain a specific pig. Each cell contains a value reflecting whether the space is occupied or not.

    public float delay;                     // Time to wait in order to give other processes time to finalize
    public float waitTime;                  // Time to wait for an answer before going on with other matters.
    public float blinkInterval;             // Time between blinks when flashing lines, texts etc.
    public float speedFactor;               // The value used to calculate the speed. It is reciprocal as it specifies the factor to decrease the time between a pig falls 1 cell.
    public float speed;                     // Time in seconds between the pig falls 1 cell.
    public float startX;                    // Pixel reference (WorldPosition) to start of location on screen where the play area start.
    public float startY;                    // Pixel reference (WorldPosition) to where the playfield begins (where the pigs can fall no more).
    public float cellWidth;                 // Width of a cell in pixels (WorldPosition).
    public float cellHeigth;                // Heigth of a cell in Pixels (WorldPosition).
    public float pauseTime = 0f; 

    public bool debug;
    public bool gameOver = false;           // Flag/Switch for when game is over. (When a spawned pig cannot move when doing first move
    public bool pigFalling = false;         // Flag indicating whether a pig is currently falling (and NOT stuck)
    public bool gamePaused = false;         // Flag indicating that the game is paused during game end. We are not setting timescale to 0 as that halts coroutines.
    private bool fullLineRunning = false;   // Flag set when fullline coroutine is running
    private bool winner = false;            // Flag set when game is over if the score is eligible for the leaderboard
    private bool stuckedPigsRunning = false;
    private bool increaseLevelRunning = false;
    private bool weAreInTheBackGround = false;
   public string PlayerName;
    private string filePathLanguageVersion;
    int stuckedPigNumber = 0;
    int temp;

    #endregion

    #region Unity Methods
    void Start()
    {
        QualitySettings.vSyncCount = 1;
        _highScoreController = _highScore.GetComponent<highscoreController>();
        _highScoreController.SelfStart();
        filePathLanguageVersion = Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameLanguageVersion;
      
        pigsToAdjust = new List<GameObject>();
        soundController = GameObject.Find("SoundEffectsController").GetComponent<SoundEffectsController>();

        NewGame();
    }
    private void OnApplicationFocus(bool focus)
    {
        if (focus)     // app returning from background
        {

            if ((Time.realtimeSinceStartup) > (pauseTime + Constants.pauseInterval))
            {
                _background.SetActive(true);
                _introManager.SetActive(true);
            }
            else
            {   Time.timeScale = 1f;
                soundController.setVolume();
            }
        }
        else // app going to the background
        {
            soundController.silence();
            pauseTime = Time.realtimeSinceStartup;
            _background.SetActive(false);
            Time.timeScale = 0f;         
        }

    }

    private void Update()               // Checks whether the game is over, if over starts coroutines to do the bookkeeping
    {
        if (gameOver)
        {
            pigFalling = true;          // In order to prevent a new pig being spawned until we are done with bookkeeping we set pigfalling to true
            gameOver = false;           // Same logic, in order not to repeatedly call gameover we set this to false until we are done with bookkeeping
            StopAllCoroutines();        // Whatever coroutines we running (falling pigs, chceking for lines etc...) are to stop
            soundController.stopMusic();
            _miniPig.SetActive(false);
            _pauseButton.SetActive(false);
            _closeButton.SetActive(false);
            _levelText.SetActive(false);
            _playerText.SetActive(false);
            winner = _highScoreController.CheckHighScore(score);

            if (winner)
            {
                _winnerScreen.SetActive(true);
            }
            else
            {
                _loserScreen.SetActive(true);
            }

            PlayerPrefs.SetInt("highScoreScreen", 1);
            _scoreText.transform.localScale = (new Vector3(2f, 2f, 1f));
 //           _scoreText.transform.localPosition = _scoreText.transform.localPosition + new Vector3(0f, -129f, 0f);
            //            _playerText.SetActive(false);

            //  _scoreText.transform.localScale = (new Vector3(1f, 1f, 1f));

            //           HardXit();

        }
//        Debug.Log(" timescale " + Time.timeScale);
    }
    void FixedUpdate()                  // If no pigs are falling and game is not over, the pig is stucked, do house keeping and start next spawn
    {
        if (!pigFalling && !gameOver && !stuckedPigsRunning)
        {
            pigFalling = true;
            stuckedPigsRunning = true;
            StartCoroutine("StuckedPig");
        }
    }
    #endregion
 
    #region Button methods
    public void OnPause()
    {
        Time.timeScale = 0f;
        _pauseScreen.SetActive(true);
        _unPauseButton.SetActive(true);
    }           // Action Pause button tapped
    public void UnPause()
    {
        _pauseScreen.SetActive(false);
        _unPauseButton.SetActive(false);
        Time.timeScale = 1f;
    }           // Action when close button on pause screen tapped
    public void Xit()
    {
        StartCoroutine("AreYouSure");
    }                   // Action when close/(X) tapped on game screen, soft exit (are you sure) warning to avoid stopping game by accident
    public void RestartFromSleep()
    {
        // restart sound and ads
        soundController.setVolume();
        Time.timeScale = 1.0f;
        
    }       

    public void HardXit()
    {
//        adController.DestroyBanner();
        StartCoroutine("ReturnToMainMenu");
    }               // Hard Exit, Exit and Straigth to MainMenu
    IEnumerator AreYouSure()
    {
        gamePaused = true;
        _areYouSure.SetActive(true);
        yield return new WaitForSeconds(waitTime);
        _areYouSure.SetActive(false);
        gamePaused = false;
        yield return null;
    }
    IEnumerator ReturnToMainMenu()
    {
        StaticClass.pigTrisCalling = true;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainMenu");
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
    #endregion
    void InitializePlayField()          // Mark all cells empty
    {
        _playFieldMatrix = new GameObject[playFieldWidth, playFieldHeigth];
        for (int i = 0; i < playFieldWidth; i++)
        {
            for (int j = 0; j < playFieldHeigth; j++)
            {
                _playFieldMatrix[i, j] = null;
            }
        }
        _scoreText.SetActive(true);
        _levelText.SetActive(true);
        _playerText.GetComponent<Text>().text = PlayerPrefs.GetString("playerName");
        _playerText.SetActive(true);
    }

    #region Level Accounting

    void StartNewLevel()
    {
        ClearAllPigs();
        InitializePlayField();
        UpdateScore();
        UpdateLevel();
        nextPig = Random.Range(0, 7); // choose random pig 
        SpawnPig();
        pigFalling = true;
        _miniPig.SetActive(true);
        _closeButton.SetActive(true);
        _newLevelReached.SetActive(false);
    }
    IEnumerator IncreaseLevel()       // Co/routine actually performing much of the checklevel stuff as it is timeconsuming.
    {
        increaseLevelRunning = true;
        _newLevelReached.GetComponent<Text>().text = "Level " + level.ToString() + " reached";

        for (int i = 0; i < 11; i++)
        {
            _newLevelReached.SetActive(!_newLevelReached.activeSelf);
            yield return new WaitForSeconds(blinkInterval);
        }
        increaseLevelRunning = false;
        yield return null;

    }
    public void NewGame()               // Starting a new game, called from play again button.
    {
        StopAllCoroutines();
        _gameOverText.SetActive(false);
        InitializePlayField();
        ClearAllPigs();
        score = 0;
        level = 1;
        UpdateScore();
        UpdateLevel();
        nextPig = Random.Range(0, 7); // choose random pig     
        StartCoroutine("RandomSfx");
        SpawnPig();
        pigFalling = true;
        gameOver = false;
        _miniPig.SetActive(true);
        _closeButton.SetActive(true);
    }
    void ClearAllPigs()
    {
        pigsToAdjust.Clear();
        for (int i = 0; i < playFieldWidth; i++)
        {
            for (int j = 0; j < playFieldHeigth; j++)
            {
                if (_playFieldMatrix[i, j] != null && !pigsToAdjust.Contains(_playFieldMatrix[i, j]))
                {
                    pigsToAdjust.Add(_playFieldMatrix[i, j]);
                }
            }
        }
        foreach (var pig in pigsToAdjust)
        {
            Destroy(pig);
        }
    }               // Clears ALL pigs from the playfield
    #endregion 
    IEnumerator RandomSfx()             // Random playing of Sound effects (pig farts and pig grunts)
    {
        yield return new WaitForSeconds(Random.Range(1.0f, 3.0f));
        while (true)
        {
            if (Random.Range(0, 3) == 1)
                soundController.playPrutter();
            else
                soundController.playGrynt();

            yield return new WaitForSeconds(Random.Range(1.5f, 4.5f));
        }
    }
    void SpawnPig()                     // As long as game runs a new pig is spawned when a pig gets stucked stucked
    {
        score++;                                     // Add points to the score, 1 * level number for each new pig spawned.
        UpdateScore();
        Instantiate(_pigsToSpawn[nextPig], this.transform);  // new pig spawned
        pigFalling = true;                                  // We have a pig falling, controlled by the controller on the pig
        nextPig = Random.Range(0, 7);                       // choose random pig to spawn when the just spawned stops
        _miniPig.GetComponent<SpriteRenderer>().sprite = nextPigMiniImage[nextPig];  // Display the image of the next pig to be spawned
    }

    #region Stucked pig Situation
    void ChangeLineStatus(bool active)
    {
        for (int i = 0; i < playFieldWidth; i++)
        {
            _playFieldMatrix[i, fullLineNumber].SetActive(active);
            _scoreText.SetActive(!active);
        }
    }
    bool CheckCompletedLines()      // We check bottum up if any line are full of pigs, if so bookkeeping, return true and stops
    {
        for (int j = 0; j < playFieldHeigth; j++)
        {
            if (CheckLine(j))            // Do we have a full line?
            {
                fullLineNumber = j;
                UpdatePlayFieldMatrix(j); // convert all pigs on the line to smallPigs
                return true;
            }
        }
        return false;
    }
    bool CheckLine(int j)
    {
        bool foundCompleteLine = true;

        for (int i = 0; i < playFieldWidth; i++)
        {
            if (_playFieldMatrix[i, j] == null)
            {
                foundCompleteLine = false;
            }
        }
        return foundCompleteLine;
    }
    void ConvertPig()                   // Converts all cells occupied by a pig with a small pig (1 cell in footprint)
    {
        float pigCenter = (currentPigMatrix.GetLength(0) / 2f);
        float xLowerLeft = pigController.transform.position.x - (pigCenter * cellWidth); // calculation of the  x position in the fieldMatrix, taken from the object pivot point, start lower left corner
        float yLowerLeft = pigController.transform.position.y - (pigCenter * cellHeigth); // calculation of the y position in the fieldMatrix, taken from the object pivot point, start lower left corner

        for (int i = 0; i < currentPigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < currentPigMatrix.GetLength(0); j++)
            {
                if (currentPigMatrix[i, j] != 9)
                {
                    GameObject smallPigToSpawn;
                    smallPigToSpawn = Instantiate(_pigsToSpawn[7], transform);
                    smallPigToSpawn.transform.position = new Vector3(xLowerLeft + i * cellWidth + 32, yLowerLeft + j * cellHeigth + 32, 0);
                    int xP = (int)(smallPigToSpawn.transform.position.x - startX) / (int)cellWidth;
                    int yP = (int)(smallPigToSpawn.transform.position.y - startY) / (int)cellHeigth;
                    _playFieldMatrix[xP, yP] = smallPigToSpawn;
                }
            }
        }
    }
    void DestroyPigsOnLine()            // Destroying all of the spawned pigs (gameobjects) on the line to be deleted and adjusted all of the remaining pigs objects y position 1 cell down
    {
        pigsToAdjust.Clear();
        for (int i = 0; i < playFieldWidth; i++)
        {
            Destroy(_playFieldMatrix[i, fullLineNumber]);
            _playFieldMatrix[i, fullLineNumber] = null;
            for (int j = fullLineNumber + 1; j < playFieldHeigth; j++)
            {
                if (_playFieldMatrix[i, j] != null && !pigsToAdjust.Contains(_playFieldMatrix[i, j]))
                {
                    pigsToAdjust.Add(_playFieldMatrix[i, j]);
                }
                _playFieldMatrix[i, j - 1] = _playFieldMatrix[i, j];
            }
            _playFieldMatrix[i, playFieldHeigth - 1] = null;
        }
        foreach (var pig in pigsToAdjust)
        {
            pig.transform.Translate(0, -cellHeigth, 0);
        }
    }
    IEnumerator FullLine()      // Started whenever there is a full line, all pigs on the line are destroyed, all lines above are moved down and new line added at top of playfield, scores tallied.
    {
        fullLineRunning = true;
        bool active = false;
        score += completedLineBonus * level + (numberOfCompletedLines - 1) * extraLineBonus;
        UpdateScore();

        for (int i = 0; i < 11; i++) // makes the line blink t show it is full and has been counted
        {
            ChangeLineStatus(active);
            active = !active;
            yield return new WaitForSeconds(blinkInterval);
        }

        DestroyPigsOnLine();

        score += completedLineBonus * level + lineBonusMultiplikator * (numberOfCompletedLines - 1) * extraLineBonus;
        UpdateScore();
        // here let them fall

        pigsToAdjust.Clear();

        for (int i = 0; i < playFieldWidth; i++)
        {
            for (int j = 0; j < playFieldHeigth; j++)
            {
                if (_playFieldMatrix[i, j] != null && !pigsToAdjust.Contains(_playFieldMatrix[i, j]))
                {
                    pigsToAdjust.Add(_playFieldMatrix[i, j]);

                    PigController PigC = _playFieldMatrix[i, j].GetComponent<PigController>();

                    if (!gamePaused)
                    {
                        while (PigC.AdjustStuckedPosition())
                        {
                            yield return new WaitForSeconds(delay);
                        }
                    }
                }
            }

        }


        fullLineRunning = false;
        yield return null;
    }
    IEnumerator StuckedPig()
    {
        stuckedPigNumber++;
        StopCoroutine("RandomSfx");
        numberOfCompletedLines = 0;
        yield return new WaitForSeconds(delay);

        bool keepGoing = true;
        while (keepGoing)
        {
            if (!fullLineRunning)                           // check to see if fullline coroutine is currently running, if so wait until it is finished
            {
                if (CheckCompletedLines())                  // check if a full line has been found, keep calling until a false response = all full lines found
                {
                    numberOfCompletedLines++;
                    StartCoroutine("FullLine");             // Process the full line
                }
                else
                {
                    keepGoing = false;
                }
            }          
            yield return new WaitForSeconds(delay);
        }

        yield return new WaitForSeconds(delay);

        if (score >= 1000 * level)
        {
            while (score > 1000 * level)          // check if new level
            {
                if (!increaseLevelRunning)
                {
                    level++;
                    speed *= speedFactor;
                    StartCoroutine("IncreaseLevel");
                }
                yield return new WaitForSeconds(delay);
            }
            StartNewLevel();
            yield return new WaitForSeconds(delay);
        }
        else
        {
            SpawnPig();
        }
        StartCoroutine("RandomSfx");
        stuckedPigsRunning = false;
        yield break;
    }
    void UpdatePlayFieldMatrix(int j)  // Bookkeeping, replacing with small pigs, destroying the big pigs and keep books of what we did.
    {
        for (int i = 0; i < playFieldWidth; i++)
        {
            if (_playFieldMatrix[i, j] != null)
            {
                pigToProcess = _playFieldMatrix[i, j];
                pigController = pigToProcess.GetComponent<PigController>();
                currentPigMatrix = pigController.ReturnPig();               // Getting the footprint of the pig in question

                if (pigController.pigType != 7)                             // Pigtype 7 is the small pig, do not want to destroy what we just added.
                {
                    ConvertPig();
                    GameObject.Destroy(pigToProcess);
                }
            }
        }
        soundController.playHyl();
    }
    #endregion 

    #region UI update methods
     void UpdateLevel()
    {
        _levelText.GetComponent<Text>().text = "Level " + level.ToString();
    }
    void UpdateScore()
    {
        _scoreText.GetComponent<Text>().text = score.ToString() + " Points";
    }

    #endregion
}
