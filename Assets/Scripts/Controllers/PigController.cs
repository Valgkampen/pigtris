using UnityEngine;
using System.Collections;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class PigController: MonoBehaviour
{     /* This class ....
    * 
    * 
    * Copyrigth Animgram Aps 2020, developed by Tom Vedel
    */
    /* 
    This control the falling pig, from randomizing position and roation when awaking, to move left, rigth, down, rotate counter clockwise (left) and rotate
    clockwise, first step in any move is to check if there are room to move id est are there any obstacles in the way.

    If there are obstacles going down, the falling is suspended & it stops listen to swipes. The GameController is notified by setting FallingPig variable
    in GameController to false and the pig stops.
    
    If there are obstacles for the other moves, it just does not move. Idea Throw a PigSound like a Pig Scream, marking it cannot move.
    
     */
    private Camera cam;

    int xPosition;  // The x position in the PlayFieldMatrix, start lower left corner. Calculated from the object pivot point.
    int yPosition;  // The y position in the PlayFieldMatrix, start lower left corner. Calculated from the object pivot point.

    public int pigType; // The actual pig, there are 8 different types numbered from 0 to 7

    int centerOffSet; // The distance from center to Lower Left corner of the pig. All Pigs Described in location of cell (int x, int y), not Pixels.

    public int[,] pigMatrix; // Description of the pig matrix (2x2, 3x3 or 4x4), each cell corresponds to a cell in the PlayFieldMatrix.
                             // Updated with current rotation.
    public int[,] tempPigMatrix; // Description of the pig matrix (2x2, 3x3 or 4x4), rotated to the position. Before a rotation the new rotated position (tempPigMatrix)
                                 // is checked to see if anything is in the way. If it can be rotated, pig is rotated and pigMatrix updated (taking the position from temPigMatrix)

    public GameObject pigSprite; // The visual Pig on the PlayField
    
    public GameObject txt;
    
    public GameController scriptGameController; // reference to the gameController script

    bool pigStucked = false;
    public bool debug;

    public void Start()
    {
        StopAllCoroutines();

        cam = Camera.main;

        scriptGameController = transform.parent.GetComponent<GameController>(); // get reference to the GameController on the Game object

        /* need to rotate the object random between 0 90 180 and 270 as start position.*/

        InitializePig();  // Initialising of the pig array depending on the pigType, 9 designate empty field, any other number refers to type of pig.
        if (pigType != 7)
        {
            int tx; // local temp value for random offset in x position
            float spawnX;
            float spawnY;

            switch (pigType) // Pigs are different sized. Need to take that into account when randomizing spawn position.                     
            {
                case 0:
                    tx = Random.Range(4, 5);
                    spawnX = scriptGameController.startX + (tx) * scriptGameController.cellWidth;
                    spawnY = 19 * scriptGameController.cellHeigth + scriptGameController.startY;
                    break;
                case 1:
                    tx = Random.Range(4, 5);
                    spawnX = scriptGameController.startX + (tx) * scriptGameController.cellWidth;
                    spawnY = 19 * scriptGameController.cellHeigth + scriptGameController.startY;
                    break;
                default:
                    tx = Random.Range(4, 5);
                    spawnX = scriptGameController.startX + (0.5f + tx) * scriptGameController.cellWidth;
                    spawnY = 20.5f * scriptGameController.cellHeigth + scriptGameController.startY;
                    break;
            }
            this.transform.position = new Vector3(spawnX, spawnY, 0);

            SwipeManager.OnSwipeDetected += OnSwipeDetected; // subscribe to the swipe events from the SwipeManager

            centerOffSet = pigMatrix.GetLength(0) / 2;

            StartCoroutine(PigFalling());  // coroutine to keep the pig falling at regular intervals 
        }

    }
    void OnDestroy()
    {
        SwipeManager.OnSwipeDetected -= OnSwipeDetected;
        StopAllCoroutines();
    }

    void AddPigToFieldMatrix()
    {
        UpdatePigPosition();
        for (int i = 0; i < pigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < pigMatrix.GetLength(0); j++)
            {
                if (pigMatrix[i, j] == pigType)
                {
                    scriptGameController._playFieldMatrix[xPosition + i, yPosition + j] = transform.gameObject;
                }
            }
        }
    }
    public bool AdjustStuckedPosition()
    {
        UpdatePigPosition();
        if (CanMoveDown())
        {
            ClearPigFromFieldMatrix();
            MoveDown();
            AddPigToFieldMatrix();
            return true;
        }
        else
            return false;
    }
    void DisplayPigsToAdjust()        // test method
    {
        GameObject temp;
        for (int j = 0; j < pigMatrix.GetLength(0); j++)
        {
            for (int i = 0; i < pigMatrix.GetLength(0); i++)
            {

                //txt.GetComponent<Text>().text = pigMatrix[i,j].ToString();
                temp = Instantiate(txt, transform);
                temp.transform.position = (new Vector3((float)(scriptGameController.startX + (xPosition) * scriptGameController.playFieldWidth), (float)(scriptGameController.startY + (yPosition + 0.5) * scriptGameController.playFieldHeigth), 0f));
            }
        }
        
    }
    bool CanMoveDown()
    {
        for (int i = 0; i < pigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < pigMatrix.GetLength(0); j++)
            {
                if (pigMatrix[i, j] == pigType)
                {
                    if (yPosition + j - 1 < 0 || scriptGameController._playFieldMatrix[xPosition + i, yPosition + j - 1] != null)
                    {
                        return false;
                    }
                    else
                    {
                        j = pigMatrix.GetLength(0);
                    };
                }
            }
        }
        return true;
    }
 
    bool CanMoveLeft()
    {
        for (int j = 0; j < pigMatrix.GetLength(0); j++)
        {
            for (int i = 0; i < pigMatrix.GetLength(0); i++)
            {
                if (pigMatrix[i, j] == pigType)
                {
                    if ((xPosition + i - 1 < 0) || scriptGameController._playFieldMatrix[xPosition + i - 1, yPosition + j] != null)
                    {
                        return false;
                    }
                    else
                    {
                        i = pigMatrix.GetLength(0);
                    }
                }
            }
        }
        return true;
    }
    bool CanMoveRigth()
    {
        for (int j = 0; j < pigMatrix.GetLength(0); j++)
        {
            for (int i = pigMatrix.GetLength(0) - 1; i >= 0; i--)
            {
                if (pigMatrix[i, j] == pigType)
                {
                    if ((xPosition + i + 1 >= scriptGameController.playFieldWidth) || scriptGameController._playFieldMatrix[xPosition + i + 1, yPosition + j] != null)
                    {
                        return false;
                    }
                    else
                    {
                        i = -1;
                    }
                }
            }
        }
        return true;
    }
    void CannotMoveDown()
    {
        SwipeManager.OnSwipeDetected -= OnSwipeDetected;
        AddPigToFieldMatrix();
        UpdatePigPosition();
        if (yPosition >= 18)
        {
            scriptGameController.gameOver = true;
        }
        scriptGameController.pigFalling = false;
        pigStucked = true;
        StopAllCoroutines();
    }
    bool CanRotateLeft()
    {
        RotatePigMatrixLeft();

        for (int j = 0; j < tempPigMatrix.GetLength(0); j++)
        {
            for (int i = tempPigMatrix.GetLength(0) - 1; i >= 0; i--)
            {
                if (tempPigMatrix[i, j] == pigType)
                {
                    if (xPosition + i < 0 || xPosition + i >= scriptGameController.playFieldWidth || yPosition + j < 0 || scriptGameController._playFieldMatrix[xPosition + i, yPosition + j] != null)
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }
    bool CanRotateRigth()
    {

        RotatePigMatrixRigth();

        for (int j = 0; j < tempPigMatrix.GetLength(0); j++)
        {
            for (int i = tempPigMatrix.GetLength(0) - 1; i >= 0; i--)
            {
                if (tempPigMatrix[i, j] == pigType)
                {
                    if (xPosition + i < 0 || xPosition + i >= scriptGameController.playFieldWidth || yPosition + j < 0 || scriptGameController._playFieldMatrix[xPosition + i, yPosition + j] != null)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    void ClearPigFromFieldMatrix()
    {
        UpdatePigPosition();
        for (int i = 0; i < pigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < pigMatrix.GetLength(0); j++)
            {
                if (pigMatrix[i, j] == pigType)
                {
                    scriptGameController._playFieldMatrix[xPosition + i, yPosition + j] = null;
                }
            }
        }
    }
 
    #region Initialize the pig when spawned;
    void InitializePig()
    {
        switch (pigType)
        {
            case 0: // Visually    **
                    //             **
                InitializePig0();
                break;

            case 1: // Visually    ****
                InitializePig1();
                break;

            case 2: // Visually   * 
                    //           ***
                InitializePig2();
                break;

            case 3: // Visually   ** 
                    //           **
                InitializePig3();
                break;

            case 4: // Visually  ** 
                    //            **
                InitializePig4();
                break;

            case 5: // Visually  * 
                    //           ***
                InitializePig5();
                break;

            case 6: // Visually    * 
                    //           ***
                InitializePig6();
                break;

            case 7: // Visually    * 
                InitializePig7();
                break;

            default:
                break;
        }
        InitializeTempPig();

    }
    void InitializePig0()// Visually    **
                         //             **
    {
        pigMatrix = new int[2, 2];
        pigMatrix[0, 0] = 0;
        pigMatrix[0, 1] = 0;
        pigMatrix[1, 1] = 0;
        pigMatrix[1, 0] = 0;
    }
    void InitializePig1()// Visually    ****
    {
        pigMatrix = new int[4, 4];
        pigMatrix[0, 0] = 9;
        pigMatrix[0, 1] = 1;
        pigMatrix[0, 2] = 9;
        pigMatrix[0, 3] = 9;
        pigMatrix[1, 0] = 9;
        pigMatrix[1, 1] = 1;
        pigMatrix[1, 2] = 9;
        pigMatrix[1, 3] = 9;
        pigMatrix[2, 0] = 9;
        pigMatrix[2, 1] = 1;
        pigMatrix[2, 2] = 9;
        pigMatrix[2, 3] = 9;
        pigMatrix[3, 0] = 9;
        pigMatrix[3, 1] = 1;
        pigMatrix[3, 2] = 9;
        pigMatrix[3, 3] = 9;

    }
    void InitializePig2()// Visually   * 
                         //           ***
    {
        pigMatrix = new int[3, 3];
        pigMatrix[0, 0] = 2;
        pigMatrix[0, 1] = 9;
        pigMatrix[0, 2] = 9;
        pigMatrix[1, 0] = 2;
        pigMatrix[1, 1] = 2;
        pigMatrix[1, 2] = 9;
        pigMatrix[2, 0] = 2;
        pigMatrix[2, 1] = 9;
        pigMatrix[2, 2] = 9;
    }
    void InitializePig3() // Visually   ** 
                          //           **
    {
        pigMatrix = new int[3, 3];
        pigMatrix[0, 0] = 3;
        pigMatrix[0, 1] = 9;
        pigMatrix[0, 2] = 9;
        pigMatrix[1, 0] = 3;
        pigMatrix[1, 1] = 3;
        pigMatrix[1, 2] = 9;
        pigMatrix[2, 0] = 9;
        pigMatrix[2, 1] = 3;
        pigMatrix[2, 2] = 9;
    }
    void InitializePig4() // Visually  ** 
                          //            **
    {
        pigMatrix = new int[3, 3];
        pigMatrix[0, 0] = 9;
        pigMatrix[0, 1] = 4;
        pigMatrix[0, 2] = 9;
        pigMatrix[1, 0] = 4;
        pigMatrix[1, 1] = 4;
        pigMatrix[1, 2] = 9;
        pigMatrix[2, 0] = 4;
        pigMatrix[2, 1] = 9;
        pigMatrix[2, 2] = 9;
    }
    void InitializePig5() // Visually  * 
                          //           ***
    {
        pigMatrix = new int[3, 3];
        pigMatrix[0, 0] = 5;
        pigMatrix[0, 1] = 5;
        pigMatrix[0, 2] = 9;
        pigMatrix[1, 0] = 5;
        pigMatrix[1, 1] = 9;
        pigMatrix[1, 2] = 9;
        pigMatrix[2, 0] = 5;
        pigMatrix[2, 1] = 9;
        pigMatrix[2, 2] = 9;
    }
    void InitializePig6() // Visually    * 
                          //           ***
    {
        pigMatrix = new int[3, 3];
        pigMatrix[0, 0] = 6;
        pigMatrix[0, 1] = 9;
        pigMatrix[0, 2] = 9;
        pigMatrix[1, 0] = 6;
        pigMatrix[1, 1] = 9;
        pigMatrix[1, 2] = 9;
        pigMatrix[2, 0] = 6;
        pigMatrix[2, 1] = 6;
        pigMatrix[2, 2] = 9;
    }
    void InitializePig7() // Visually    * 
    {
        pigMatrix = new int[1, 1];
        pigMatrix[0, 0] = 7;
    }
    void InitializeTempPig()
    {
        tempPigMatrix = new int[pigMatrix.GetLength(0), pigMatrix.GetLength(0)];
        for (int i = 0; i < tempPigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < tempPigMatrix.GetLength(0); j++)
            {
                tempPigMatrix[i, j] = 9;
            }

        }
    }
    #endregion
    void MoveDown()
    {
        transform.Translate(0, -scriptGameController.cellHeigth, 0);
    }
    IEnumerator MoveDownFast()
    {
        while (scriptGameController.pigFalling)
        {
            if (!scriptGameController.gamePaused)
            {
                UpdatePigPosition();

                if (CanMoveDown())
                    MoveDown();
                else
                    CannotMoveDown();

                yield return new WaitForSeconds(0.025f);
            }
        }
    }
    void MoveLeft()
    {
        transform.Translate(-scriptGameController.cellWidth, 0, 0);
    }
    void MoveRigth()
    {
        transform.Translate(scriptGameController.cellWidth, 0, 0);
    }
    void OnSwipeDetected(Swipe direction)
    {
        UpdatePigPosition();
        switch (SwipeManager.getSwipe(direction))
        {
            case "Left":
                if (CanMoveLeft())
                    MoveLeft();

                break;

            case "Rigth":
                if (CanMoveRigth())
                    MoveRigth();
                break;

            case "Up":

                if (cam.ScreenToWorldPoint(new Vector3(SwipeManager.startPos.x, 0, 0)).x > transform.position.x)
                {
                    if (CanRotateLeft())
                        RotateLeft();
                    break;
                }
                else
                {
                    if (CanRotateRigth())
                        RotateRigth();
                    break;
                }

            case "Down":
                if (cam.ScreenToWorldPoint(new Vector3(SwipeManager.startPos.x, 0, 0)).x <= transform.position.x)
                {
                    if (CanRotateLeft())
                        RotateLeft();
                    break;
                }
                else
                {
                    if (CanRotateRigth())
                        RotateRigth();
                    break;
                }

            case "Tap":
                Vector3 tapPos = cam.ScreenToWorldPoint(new Vector3(SwipeManager.startPos.x, SwipeManager.startPos.y, 0));

                if (tapPos.x > transform.position.x - 90 && tapPos.x < transform.position.x + 90 &&
                    tapPos.y > transform.position.y - 90 && tapPos.y < transform.position.y + 90)
                {
                    StopCoroutine("PigFalling");
                    StartCoroutine("MoveDownFast");
                }
                break;

            default:
                break;
        }
    }
    IEnumerator PigFalling()
    {

        //      yield return new WaitForSeconds(0.05f);
        while (true)
        {

            if (!scriptGameController.gamePaused)
            {

                UpdatePigPosition();
                if (CanMoveDown())
                    MoveDown();
                else
                    CannotMoveDown();
            }
            yield return new WaitForSeconds(scriptGameController.speed);
        }
    }
    public int[,] ReturnPig()
    {
        return pigMatrix;
    }
    void RotateLeft()
    {
        pigSprite.transform.rotation *= Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
        UpdatePigRotation();
    }
    void RotatePigMatrixLeft()
    {
        for (int i = 0; i < pigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < pigMatrix.GetLength(0); j++)
            {
                tempPigMatrix[pigMatrix.GetLength(0) - 1 - j, i] = pigMatrix[i, j];
            }
        }
    }
    void RotatePigMatrixRigth()
    {
        for (int i = 0; i < pigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < pigMatrix.GetLength(0); j++)
            {
                tempPigMatrix[i, j] = pigMatrix[pigMatrix.GetLength(0) - 1 - j, i];
            }
        }
    }
    void RotateRigth()
    {
        pigSprite.transform.rotation *= Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
        UpdatePigRotation();
    }

    void UpdateFieldMatrixStucked()
    {
        UpdatePigPosition();
        for (int i = 0; i < pigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < pigMatrix.GetLength(0); j++)
            {
                if (pigMatrix[i, j] == pigType)
                {
                    scriptGameController._playFieldMatrix[xPosition + i, yPosition + j] = transform.gameObject;
                }
            }
        }
    }
    void UpdatePigPosition()
    {
        if (pigType == 0 || pigType == 1)
        {
            xPosition = (int)(this.transform.position.x + 0.5 * scriptGameController.playFieldWidth - scriptGameController.startX) / (int)scriptGameController.cellWidth - centerOffSet;
            yPosition = (int)((this.transform.position.y - scriptGameController.startY)) / (int)scriptGameController.cellHeigth - centerOffSet;
        }
        else
        {
            xPosition = (int)(this.transform.position.x - scriptGameController.startX) / (int)scriptGameController.cellWidth - centerOffSet;
            yPosition = (int)((this.transform.position.y - scriptGameController.startY)) / (int)scriptGameController.cellHeigth - centerOffSet;
        }
    }
    void UpdatePigRotation()
    {
        for (int i = 0; i < tempPigMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < tempPigMatrix.GetLength(0); j++)
            {
                pigMatrix[i, j] = tempPigMatrix[i, j];
            }
        }
    }

 
 



 
 
}

