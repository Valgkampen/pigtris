﻿using UnityEngine;
using System.Collections;
using Amazon.S3;
using Amazon.Runtime;
using System.IO;
using Amazon.CognitoIdentity;
using Amazon;
using Amazon.S3.Transfer;

public class PigTrisConfigurationManager : MonoBehaviour
    //   ConfigurationManger controls the version of the app and updates the showing of infomercials when the app start and restart
{
    private Vfile vfileFromDevice;               // when updating from s3 the latest current vfile will be saved locally 
    public LanguageVersion languageVersion;             // the relevant list of images etc. to be displayed
    public LanguageVersion globalLanguageVersion;
    public GameObject introManager;
    public GameObject background;
    float pauseTime = 0;
    public bool debug = true;

    private void OnApplicationFocus(bool focus)
    {
 
        if (focus)
        {
                    pauseTime = 0;
                    background.SetActive(false);
        }
        else
        {
            pauseTime = Time.realtimeSinceStartup;
        }

      }
   

}
