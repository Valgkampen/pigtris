﻿using UnityEngine;
using System.Collections;
using System.IO;
public class IntroManagerPT : MonoBehaviour
{
    private string filePathLanguageVersion;
    private string filePathImage;
    LanguageVersion languageVersion;
    public string hitLink;
    public GameObject background;
    public GameObject adsobject;
    public GameController gameController;
    AdScript adscript;
    public bool debug;

    private void OnEnable()
    {
        adscript = adsobject.GetComponent<AdScript>();
        adscript.DestroyView();

        filePathLanguageVersion = Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameLanguageVersion;

        if (File.Exists(filePathLanguageVersion))
        {
            StartCoroutine(IntroAnimgramercial());
        }
        else
        {
            StartCoroutine(DefaultAnimgramercial());
        }

    }
    void Update()
    {
        {
            if (Input.GetMouseButtonDown(0))
            {
                Application.OpenURL(hitLink);
            }
        }
    }
    IEnumerator IntroAnimgramercial()
    {
        languageVersion = JsonUtility.FromJson<LanguageVersion>(System.IO.File.ReadAllText(filePathLanguageVersion));

        yield return new WaitForSecondsRealtime(ShowAnimgramImage());
        adscript.LoadView();
        background.SetActive(false);
        Time.timeScale = 1f;
        gameController.RestartFromSleep();
        this.gameObject.SetActive(false);
        yield return null;
    }        
    int ShowAnimgramImage()
    {
        Texture2D tex2D;
        byte[] fileData;
        int i = UnityEngine.Random.Range(0, languageVersion.AnimgramInfoImages.Length);

        filePathImage = Application.persistentDataPath +"/" + Constants.appId + "/Images/" + languageVersion.AnimgramInfoImages[i].ImageFileName;
        if (File.Exists(filePathImage))
        {
            fileData = File.ReadAllBytes(filePathImage);
            tex2D = new Texture2D(Constants.imageLength, Constants.imageHeight);
            if (tex2D.LoadImage(fileData))
            {
                GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex2D, new Rect(0, 0, Constants.imageLength, Constants.imageHeight), new Vector2(0, 0));
#if UNITY_ANDROID
                hitLink = languageVersion.AnimgramInfoImages[i].URLAndroid;
#elif UNITY_IPHONE
                hitLink = languageVersion.AnimgramInfoImages[i].URLIos;
#else           
                hitLink = "https://animgram.com/";
#endif
            }
        }
        return languageVersion.AnimgramInfoImages[i].Duration;
    }
    IEnumerator DefaultAnimgramercial()
    {
 
#if UNITY_ANDROID
        string filePathDefaultImage = System.IO.Path.Combine(Application.streamingAssetsPath, Constants.defaultImageName);
        var unityWebRequestTexture = UnityEngine.Networking.UnityWebRequestTexture.GetTexture(filePathDefaultImage);
        yield return unityWebRequestTexture.SendWebRequest();
        var fileData = unityWebRequestTexture.downloadHandler.data;
        var tex2D = new Texture2D(Constants.imageLength, Constants.imageHeight);

        if (tex2D.LoadImage(fileData))
        {
            GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex2D, new Rect(0, 0, Constants.imageLength, Constants.imageHeight), new Vector2(0, 0));
            hitLink = "https://www.animgram.com";
        }
#else
        string filePathDefaultImage = System.IO.Path.Combine(Application.streamingAssetsPath, Constants.defaultImageName);
        Texture2D tex2D;
        byte[] fileData;
        if (File.Exists(filePathDefaultImage))
        {
            fileData = System.IO.File.ReadAllBytes(filePathDefaultImage);
            tex2D = new Texture2D(Constants.imageLength, Constants.imageHeight);
            if (tex2D.LoadImage(fileData))
            {
                GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex2D, new Rect(0, 0, Constants.imageLength, Constants.imageHeight), new Vector2(0, 0));
                hitLink = "https://www.animgram.com";
            }
        }
#endif
        yield return new WaitForSecondsRealtime(Constants.defaultWait);
        GetComponent<SpriteRenderer>().sprite = null;
        background.SetActive(false);
        adscript.LoadView();
        Time.timeScale = 1f;
        gameController.RestartFromSleep();
        this.gameObject.SetActive(false);
        yield return null;
    }
}
