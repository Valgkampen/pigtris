﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highscoreController : MonoBehaviour
{
    /* This class ....
     * 
     * 
     * Copyrigth Animgram Aps 2020, developed by Tom Vedel
     */
    public GameObject _highScoreEntry;
    public Transform _entries;
    public List<GameObject> _highScoreEntries;
    highScores _highScore;
    string _convertedToJson;

    public void SelfStart()
    {
        _highScoreEntries = new List<GameObject>();
        _convertedToJson = PlayerPrefs.GetString("highScore");

        if (_convertedToJson != "" && _convertedToJson != null)
        {
            _highScore = JsonUtility.FromJson<highScores>(_convertedToJson);
        }
        else
        {
            InitializeHighscore();
        }

    }
    public bool CheckHighScore(int myscore)
    {
        _convertedToJson = PlayerPrefs.GetString("highScore");

        _highScore = JsonUtility.FromJson<highScores>(_convertedToJson);

        if (_highScore.hsList.Count >= 10)
        {
            if (myscore > _highScore.hsList[9].score)
            {
                return true;
            }
            else
            {
                return false;
            }
        } else
            return true;
        //       SortAndSaveHighscore();
    }
    public void InitializeHighscore()
    {
        _highScore = new highScores();
        _convertedToJson = JsonUtility.ToJson(_highScore);
        PlayerPrefs.SetString("highScore", _convertedToJson);
    }
    public void ClearLeaderBoard()
    {
        for (int i = _highScoreEntries.Count-1; i >= 0; i--)
        {
        var obj = _highScoreEntries[i];
        Destroy(obj);
        _highScoreEntries.RemoveAt(i);
        }
    }
    public void AddSortAndSaveHighscore(int myscore)
    {
        _highScore.hsList.Add(new ScoreEntry { name = PlayerPrefs.GetString("playerName"), score = myscore });
        for (int i = 0; i < _highScore.hsList.Count; i++)
        {
            for (int j = i + 1; j < _highScore.hsList.Count; j++)
            {
                if (_highScore.hsList[j].score > _highScore.hsList[i].score)
                {
                    ScoreEntry tmp = _highScore.hsList[i];
                    _highScore.hsList[i] = _highScore.hsList[j];
                    _highScore.hsList[j] = tmp;
                }
            }
        }
        _convertedToJson = JsonUtility.ToJson(_highScore);
        PlayerPrefs.SetString("highScore", _convertedToJson);
    }
    public void BuildLeaderBoard()
    {
        int limit = 10;

        if (_highScore.hsList.Count < 10)
            limit = _highScore.hsList.Count;

        for (int i = 0; i < limit; i++)
        {
            if (i < _highScore.hsList.Count && _highScore.hsList[i].score > 0)
            {
                GameObject obj = Instantiate(_highScoreEntry, new Vector3(0.32f, -0.5f + (-0.65f * i), 0), new Quaternion(0, 0, 0, 0), _entries);
                obj.transform.Find("posText").GetComponentInChildren<Text>().text = (i + 1).ToString();
                obj.transform.Find("nameText").GetComponentInChildren<Text>().text = _highScore.hsList[i].name;
                obj.transform.Find("scoreText").GetComponentInChildren<Text>().text = _highScore.hsList[i].score.ToString();
                _highScoreEntries.Add(obj);
            }
            else
            {
                i = limit;
            }
        }
    }
    [System.Serializable]
    private class ScoreEntry
    {
        public string name;
        public int score;
    }
    [System.Serializable]
    private class highScores
    {
        public List<ScoreEntry> hsList;
        public highScores()
        {
            hsList = new List<ScoreEntry>();
        }
    }

}
