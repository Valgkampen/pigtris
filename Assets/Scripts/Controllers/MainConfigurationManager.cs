﻿using UnityEngine;
using System.Collections;
using Amazon.S3;
using Amazon.Runtime;
using System.IO;
using Amazon.CognitoIdentity;
using Amazon;
using Amazon.S3.Transfer;

public class MainConfigurationManager : MonoBehaviour
//   ConfigurationManger controls the version of the app and updates the showing of infomercials when the app start and restart
{
    static string deviceLanguage = "";
    [SerializeField] Vfile _vfileFromS3;                    // _vfile from s3 is the latest vfile published and residing on s3
    Vfile _vfileFromDevice;               // when updating from s3 the latest current vfile will be saved locally 
    [SerializeField] LanguageVersion _languageVersion;             // the relevant list of images etc. to be displayed
    [SerializeField] LanguageVersion _globalLanguageVersion;
    [SerializeField] GameObject _introManager;
    [SerializeField] GameObject _updateAppManager;
    [SerializeField] GameObject _background;
    [SerializeField] GameObject _setPlayerName;
    [SerializeField] GameObject _mainMenu;
    public string updateLink;
    flags _flags;
    float _pauseTime = 0;

    #region This is all the IO, Filehandling and S3 AWS.Net API Stuff

    public string IdentityPoolId = null;
    private string CognitoIdentityRegion = RegionEndpoint.USEast1.SystemName;

    private RegionEndpoint _CognitoIdentityRegion
    {
        get { return RegionEndpoint.GetBySystemName(CognitoIdentityRegion); }
    }

    private string S3Region = RegionEndpoint.USEast1.SystemName;
    private RegionEndpoint _S3Region
    {
        get { return RegionEndpoint.GetBySystemName(S3Region); }
    }

    private AWSCredentials _credentials;
    private AWSCredentials Credentials
    {
        get
        {
            if (_credentials == null)
                _credentials = new CognitoAWSCredentials(IdentityPoolId, _CognitoIdentityRegion);
            return _credentials;
        }
    }

    private IAmazonS3 _s3Client;
    private IAmazonS3 S3Client
    {
        get
        {
            if (_s3Client == null)
            {
                _s3Client = new AmazonS3Client(Credentials, _S3Region);

            }
            return _s3Client;
        }
    }
    #endregion

    private void Awake()
    {
         _flags = new flags();
    }
    private void Start()
    {

        if (!StaticClass.pigTrisCalling)
        {
            _background.SetActive(true);
            Time.timeScale = 0f;
            string filePathVfileStreamingAssets = System.IO.Path.Combine(Application.streamingAssetsPath, Constants.fileNameVfile);
            _introManager.SetActive(true);
        }
        else
        {
            if (PlayerPrefs.GetString("playerName") == "" && (PlayerPrefs.GetInt("highScoreScreen") == 0))
                _setPlayerName.SetActive(true);
            else
                _mainMenu.SetActive(true);
        }

        StaticClass.pigTrisCalling = false;
    }
    private void OnApplicationFocus(bool focus)
    {
        var updatemanagerNotRunning = !_updateAppManager.activeSelf;
        var checkVFileNotRunning = !_flags.VfileRunning;

         if (focus && updatemanagerNotRunning)
        {
            if (checkVFileNotRunning)
                StartCoroutine(CheckVfile());

            if (!StaticClass.pigTrisCalling)
            {
                if (Time.realtimeSinceStartup > _pauseTime + Constants.pauseInterval)
                {
                    _background.SetActive(true);
                    Time.timeScale = 0f;
                    _introManager.SetActive(true);
                }
                else
                {
                    _pauseTime = 0;
                    _background.SetActive(false);
                }
            }
        }

        if (!focus)
            _pauseTime = Time.realtimeSinceStartup;
    }
    IEnumerator CheckVfile() // load the latest version from s3, if newer vfile from server update data on device
    {
        _flags.VfileRunning = true;
        _vfileFromDevice = null;
        _languageVersion = null;
        _globalLanguageVersion = null;

        LoadVfileFromS3();
        LoadVfileFromSaved();
        if ((_vfileFromS3 == null) || (_vfileFromDevice == null))
        {
            yield return new WaitForSecondsRealtime(0.2f);
        }

        if ((_vfileFromS3.Version > _vfileFromDevice.Version) && _vfileFromS3.Version != 9999)
        {
            #region This is the devicespecific response to updating the vFile from S3 to the vFile on the device
#if UNITY_ANDROID

            if (_vfileFromS3.UpdateAndroid && float.Parse(Application.version, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) < _vfileFromS3.AndroidVersionUpdate)
            {
                _background.SetActive(true);
                Time.timeScale = 0f;
                updateLink = _vfileFromS3.AndroidLink;
                _updateAppManager.SetActive(true);
            }
            else
            {
                FindNewLanguageVersion();

                while (_introManager.activeSelf)
                    yield return new WaitForSecondsRealtime(0.2f);

                _vfileFromDevice = _vfileFromS3;

                if (_languageVersion != null)
                    NewInfoImages(_languageVersion);

                SaveAllToPD();
            }
#elif UNITY_IPHONE
            if (_vfileFromS3.UpdateIos && float.Parse(Application.version, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) < _vfileFromS3.IosVersionUpdate)
            {
                 _background.SetActive(true);
                Time.timeScale = 0f;
                updateLink = _vfileFromS3.IosLink;
                _updateAppManager.SetActive(true);
            }
            else
            {
                FindNewLanguageVersion();

                while (_introManager.activeSelf)
                    yield return new WaitForSecondsRealtime(0.2f);

                _vfileFromDevice = _vfileFromS3;

                if (_languageVersion != null)
                    NewInfoImages(_languageVersion);

                SaveAllToPD();
            }

#elif UNITY_EDITOR

#endif
            #endregion
        }
        _flags.VfileRunning = false;
        yield return null;
    }
    void FindNewLanguageVersion() // Called from Check Vfile, Find the relevant languageVersion.
    {
        deviceLanguage = Utilities.GetSystemLanguageCode();

        foreach (var VFlanguageVersion in _vfileFromS3.LanguageVersions)
        {
            if (VFlanguageVersion.Language == "!G")
            {
                _globalLanguageVersion = VFlanguageVersion;
            }
            else if (VFlanguageVersion.Language == deviceLanguage)
            {
                _languageVersion = VFlanguageVersion;
            }
        }

        if (_languageVersion == null)
            _languageVersion = _globalLanguageVersion;

    }
    private void CopyVfileFromStreamingAssets() // copies vfile from streamingassets to persistentdata/appid if it is the very first time the app is runnig
    {
        if (!Directory.Exists(Application.persistentDataPath + "/" + Constants.appId))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + Constants.appId + "/");
        }

        string filePathVfileStreamingAssets = System.IO.Path.Combine(Application.streamingAssetsPath, Constants.fileNameVfile);

        if (filePathVfileStreamingAssets.Contains("://") || filePathVfileStreamingAssets.Contains(":///"))
        {
            UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(filePathVfileStreamingAssets);
            www.SendWebRequest();
            System.IO.File.WriteAllText(Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameVfile, www.downloadHandler.text);
        }
        else
        {
            System.IO.File.WriteAllText(Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameVfile, System.IO.File.ReadAllText(filePathVfileStreamingAssets));
        }
    }
    private void LoadVfileFromSaved() // Load the locally stored current vfile from Persistent Data, if first run copy from streaming assets

    {
        if (System.IO.File.Exists(Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameVfile))
        {
            _vfileFromDevice = JsonUtility.FromJson<Vfile>(System.IO.File.ReadAllText(Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameVfile));
        }
        else
        {
            CopyVfileFromStreamingAssets();

            _vfileFromDevice = JsonUtility.FromJson<Vfile>(System.IO.File.ReadAllText(Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameVfile));
        }
    }
    void LoadVfileFromS3() // Loads the vFile from server and stores it in vfileFromS3, if anything goes wron the vfile will have version number 9999
    {
        _vfileFromS3 = new Vfile();
        _vfileFromS3.Version = 9999;
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log(this.name + " Error. Check internet connection!");
        }
        else
        {
            using (TransferUtility transferUtility = new TransferUtility(S3Client))
            {
                transferUtility.Download(Application.persistentDataPath + "/temp/" + "tempvfile.json", "animgram", Constants.appId + "/" + Constants.fileNameVfile);
                _vfileFromS3 = JsonUtility.FromJson<Vfile>(System.IO.File.ReadAllText(Application.persistentDataPath + "/temp/" + "tempvfile.json"));
            }
        }
    }
    void SaveAllToPD() // If the Vfile and or/ the infomercials needs to be updated, this is called to save the update as the current version 
    {
        if (_languageVersion != null)
        {
            System.IO.File.WriteAllText(Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameVfile, JsonUtility.ToJson(_vfileFromDevice));
            System.IO.File.WriteAllText(Application.persistentDataPath + "/" + Constants.appId + "/" + Constants.fileNameLanguageVersion, JsonUtility.ToJson(_languageVersion));
        }
    }
    void NewInfoImages(LanguageVersion languageVersion) // Downloads and stores the new infoImages locally for future use.
    {
        if (Directory.Exists(Application.persistentDataPath + "/" + Constants.appId + "/Images/"))
            Directory.Delete(Application.persistentDataPath + "/" + Constants.appId + "/Images/", true);

        Directory.CreateDirectory(Application.persistentDataPath + "/" + Constants.appId + "/Images");

        foreach (var infoImage in languageVersion.AnimgramInfoImages)
        {
            var transferUtility = new TransferUtility(S3Client);
            transferUtility.Download(Application.persistentDataPath + "/" + Constants.appId + "/Images/" + infoImage.ImageFileName, "animgram", infoImage.ImageFileName);
        }

    }
}
