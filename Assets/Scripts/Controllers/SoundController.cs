using UnityEngine;
using System.Collections;

abstract public class SoundController : MonoBehaviour
{
    /* This class ....
     * 
     * 
     * Copyrigth Animgram Aps 2020, developed by Tom Vedel
     */
    protected void PlaySound(AudioClip clip, AudioSource audioSource)
    {
        if (audioSource.isPlaying)
        {
            return;
        }

        audioSource.clip = clip;
        audioSource.Play();
    }

    protected void PlayOneShotSound(AudioClip clip, AudioSource audioSource)
    {
        audioSource.PlayOneShot(clip);
    }

    protected void PlayRandomSound(AudioClip[] audioClip, AudioSource audioSource)
    {
        if (audioSource.isPlaying)
        {
            return;
        }

        audioSource.clip = audioClip[Random.Range(0, audioClip.Length)];
        audioSource.Play();
    }

    protected void PlayOneShotRandomSound(AudioClip[] audioClip, AudioSource audioSource)
    {
        audioSource.PlayOneShot(audioClip[Random.Range(0, audioClip.Length)]);
    }
}
