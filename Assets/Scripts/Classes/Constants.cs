﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static int imageHeight = 1820;                // static information on the sizes, locations and local folders
    public static int imageLength = 1024;
    public static string appId = "Pigtris";
    public static string defaultImageName = "defaultImage.png";
    public static int defaultWait = 5;
    public static string fileNameLanguageVersion = "languageVersion.json";
    public static string fileNameVfile = "vfile.json";
    public static float pauseInterval = 300.0f;
}
