﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.Runtime;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using Amazon.S3.Util;
using System.Collections.Generic;
using Amazon.CognitoIdentity;
using Amazon;
using Amazon.S3.Transfer;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Util;

[Serializable]
public class Vfile
{
    [SerializeField]
    public int Version;             // number, when it increases there are changes and you need to update in the app
    [SerializeField]
    public float AndroidVersionUpdate;
    public bool UpdateAndroid;
    public string AndroidLink;
    public float IosVersionUpdate;
    public bool UpdateIos;
    public string IosLink;
    public LanguageVersion[] LanguageVersions;
}
[Serializable]
public class LanguageVersion
{
    public string Language;         // language code, we do not use location, but language. "!G" means global default language.
    public AnimgramInfoImage[] AnimgramInfoImages; // Array of images relative to the language
}
[Serializable]
public class AnimgramInfoImage         // Basic Info for a given Infomercial 
{
    public string ImageFileName;   // filename as it appears in the s3 bucket
    public string Description;     // Short description of the image, not used, just faster identification
    public string URLAndroid;             // URL for click on frame when image is displayed
    public string URLIos;             // URL for click on frame when image is displayed
    public int Duration;           // Time to display image in seconds.
}

