﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameResult : MonoBehaviour
{
    public Text PlayerName;
    [SerializeField] highscoreController _highScoreController;
    [SerializeField] GameController _gameController;
    [SerializeField] GameObject _setPlayerName;
    int _counter = 10;
    int _score;
    // Start is called before the first frame update
    bool _stop = false;
    void Start()
    {
        PlayerName.text = PlayerPrefs.GetString("playerName");
        _score = _gameController.score;
        StartCoroutine("KeepAlive");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator KeepAlive()
    {
        while (_counter > 0)
        {
            _counter--;
            yield return new WaitForSecondsRealtime(0.5f);
        }
        _highScoreController.AddSortAndSaveHighscore(_score);
        _gameController.HardXit();
        this.gameObject.SetActive(false);
        yield return null;
    }
    public void ChangeName()
    {

        // inputfield -> playerpref playername
        PlayerName.text = PlayerPrefs.GetString("playerName");
        _counter = 2;
    }
    public void EditName()
    {
        _counter = 1000;
        _setPlayerName.SetActive(true);

     //   setactive false text
      //      setactive true inputfield

    }
}
