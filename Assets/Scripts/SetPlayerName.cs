﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class SetPlayerName : MonoBehaviour
{
    /* This class is setting up the player name. 
     * 
     * Copyrigth Animgram Aps 2021, developed by Tom Holme Vedel
     */

    [SerializeField] InputField _inputPlayerName;     // reference to the object holding the screen to enter playername
    [SerializeField] GameObject _mainMenu;    // reference to the object holding the main menu
    [SerializeField] GameObject _scoreText;
    GameResult _gameResult;

    void Start()
    {
        _inputPlayerName.text = PlayerPrefs.GetString("playerName");
        if (GameObject.FindWithTag("GameResult").GetComponent<GameResult>() != null)
        {
            _gameResult = GameObject.FindWithTag("GameResult").GetComponent<GameResult>();
        } 
        _scoreText.SetActive(false);
    }
    private void OnDisable()
    {
        _scoreText.SetActive(true);
    }
    public void OnButtonPlayerNameEnter()         // If the app has not registered a player name, it will ask for one.
    {

        PlayerPrefs.SetString("playerName", _inputPlayerName.text);
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            _mainMenu.SetActive(true);
        }
        else if (_gameResult != null)
        {

            _gameResult.ChangeName();
        }

        this.gameObject.SetActive(false);
    }
}
