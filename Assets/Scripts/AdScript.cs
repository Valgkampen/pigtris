﻿using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdScript : MonoBehaviour
{
    private BannerView bannerView;
    string adUnitId;


    public void Start()

    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(initStatus => { });

        this.RequestBanner();
        LoadView();
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID

        adUnitId = "ca-app-pub-1432748738239534/7274311281";
#elif UNITY_IPHONE

        adUnitId = "ca-app-pub-1432748738239534/7308420554";
#else
        adUnitId = "unexpected_platform";
#endif
        RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
        .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
        .build();

        MobileAds.SetRequestConfiguration(requestConfiguration);
    }
    public void DestroyView()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }

    }
    public void LoadView()
    {
        AdSize adaptiveSize =
                AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);

        this.bannerView = new BannerView(adUnitId, adaptiveSize, AdPosition.Bottom);




 //       this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        this.bannerView.LoadAd(request);
    }
}
